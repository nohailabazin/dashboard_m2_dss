# PMA Dashboard

This project is an interactive dashboard developed with Dash and Plotly to analyze forum discussions on the Fiv.fr and Doctissimo.fr websites concerning the path to Medically Assisted Procreation (MAP). The dashboard can visualize temporal and geographical data as well as generate word clouds and network graphs to better understand the psychological and social aspects of infertility.

## Live Application

Explore the live dashboard at http://162.19.154.120:8052/

## Project Overview

- **Temporal Visualization**: Display data on a time scale.
- **Geographic Analysis**: Use Folium and Geopandas to map data.
- **Word Clouds**: Generate word clouds based on textual data.
- **Network Graphs**: Create graphs to analyze relationships in data.
- **Psychological Analysis**: Visualization of the psychological aspects of infertility.

## Technologies Used

- Python
- Dash
- Plotly
- Pandas
- Folium
- Geopandas
- Scikit-learn
- NLTK
- NetworkX
- Matplotlib

## Data Files

Place the necessary data files in the appropriate directory:

- `Data/data_predictions.csv`
- `Data/preprocessed_psychopma.csv`
- `Data/data.csv`
- `Data/regions-version-simplifiee.geojson`
- `Data/pma_doctissimo_posts.csv`

## File Description

- **Scripts**: Directory containing scripts for web scraping.
- **Data**: Directory containing the data files.
- **Dash**: Directory containing the Dash application (`main.py`).

## Contributors

- **BAZINE Nohaila**
- **KANOUN Melinda**
- **KHOUITI Karima**
- **MENZOU Katia**
- **SABRI Ouahiba**


