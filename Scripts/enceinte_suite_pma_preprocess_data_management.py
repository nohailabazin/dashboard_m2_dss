import re
from collections import defaultdict
import pandas as pd
import string 

# Load the data
file_path = './enceinte_pma.csv'
data = pd.read_csv(file_path)

# List of French stopwords
french_stopwords = set([
    'alors', 'au', 'aucuns', 'aussi', 'autre', 'avant', 'avec', 'avoir', 'bon',
    'car', 'ce', 'cela', 'ces', 'ceux', 'chaque', 'ci', 'comme', 'comment', 
    'dans', 'des', 'du', 'dedans', 'dehors', 'depuis', 'devrait', 'doit', 'donc', 
    'dos', 'droite', 'début', 'elle', 'elles', 'en', 'encore', 'essai', 'est', 
    'et', 'eu', 'fait', 'faites', 'fois', 'font', 'force', 'haut', 'hors', 'ici', 
    'il', 'ils', 'je', 'juste', 'la', 'le', 'les', 'leur', 'là', 'ma', 'maintenant', 
    'mais', 'mes', 'mine', 'moins', 'mon', 'mot', 'même', 'ni', 'nommés', 'notre', 
    'nous', 'nouveaux', 'ou', 'où', 'par', 'parce', 'pas', 'peu', 'peut', 'plupart', 
    'pour', 'pourquoi', 'quand', 'que', 'quel', 'quelle', 'quelles', 'quels', 'qui', 
    'sa', 'sans', 'ses', 'seulement', 'si', 'sien', 'son', 'sont', 'sous', 'soyez', 
    'sujet', 'sur', 'ta', 'tandis', 'tellement', 'tels', 'tes', 'ton', 'tous', 'tout', 
    'trop', 'très', 'tu', 'voient', 'vont', 'votre', 'vous', 'vu', 'ça', 'étaient', 
    'état', 'étions', 'été', 'être', 'ok', 'mais', 'du', 'coup'
])

# Simplified lemmatization dictionary (example)
lemmatization_dict = defaultdict(lambda: '', {
    'filles': 'fille',
    'questions': 'question',
    'calcul': 'calculer',
    'jours': 'jour',
    'félicitations': 'féliciter',
    'allez': 'aller',
    'trop': 'très'
})

# Function to clean the text
def simple_clean_text(text):
    # Lowercase the text
    text = text.lower()
    
    # Remove punctuation
    text = re.sub(f"[{re.escape(string.punctuation)}]", " ", text)
    
    # Tokenize the text
    tokens = text.split()
    
    # Remove stopwords and lemmatize
    tokens = [lemmatization_dict[token] if lemmatization_dict[token] else token for token in tokens if token not in french_stopwords]
    
    # Join the tokens back into a single string
    cleaned_text = ' '.join(tokens)
    
    return cleaned_text

# Apply the cleaning function to the 'Contenu' column
data['Contenu_nettoye'] = data['Contenu'].apply(simple_clean_text)

# Save the cleaned data to a new CSV file
cleaned_file_path = './enceinte_pma_cleaned.csv'
data.to_csv(cleaned_file_path, index=False)

# Display the first few rows of the cleaned dataframe
print(data.head())

print("Precprocess terminé et données sauvegardées dans", cleaned_file_path)