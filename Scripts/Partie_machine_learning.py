import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score, precision_score, recall_score, f1_score
import seaborn as sns
import matplotlib.pyplot as plt
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, Input
from tensorflow.keras.callbacks import EarlyStopping, TensorBoard
import datetime
from sklearn.metrics import roc_curve, auc, confusion_matrix
from tensorflow.keras.utils import plot_model
from gensim.models import Word2Vec
import numpy as np

# Charger les données nettoyées
file_path = './enceinte_pma_cleaned.csv'
data = pd.read_csv(file_path)

# Définir les mots-clés de succès et d'échec pour l'annotation manuelle
success_keywords = ['réussite', 'excitée', 'bonne', 'succès', 'positive', 'contente', 'heureuse', 'je suis enceinte', 'positif', 'foetus', 'positifs', 'félicitations', 'joie', 'naissance', 'épanouissement', 'embryon', 'favorable']
failure_keywords = ['échec', 'perdu', 'mauvaise', 'négatif', 'triste', 'fausse couche', 'perte', 'mal', 'pleuré', 'infertilité', 'espoir', 'résilience', 'adoption', 'raté', 'désolée', 'dommage', 'défavorrable', 'déception']

# Fonction pour détecter les mots-clés dans les commentaires
def contains_keywords(text, keywords):
    if not isinstance(text, str):
        return False
    return any(keyword in text for keyword in keywords)

# Ajouter des colonnes d'indicateurs pour succès et échec
data['success'] = data['Contenu_nettoye'].apply(lambda x: 1 if contains_keywords(x, success_keywords) else 0)
data['failure'] = data['Contenu_nettoye'].apply(lambda x: 1 if contains_keywords(x, failure_keywords) else 0)

# Vérifier et ajuster les colonnes de commentaires
data['Contenu_nettoye'] = data['Contenu_nettoye'].astype(str)

# Tokeniser les commentaires
tokenized_comments = [comment.split() for comment in data['Contenu_nettoye']]

# Entraîner le modèle Word2Vec
w2v_model = Word2Vec(tokenized_comments, vector_size=100, window=5, min_count=1, workers=4)

# Créer une fonction pour obtenir le vecteur moyen d'un commentaire
def get_average_word2vec(tokens_list, model, vector_size):
    avg_vector = np.zeros(vector_size)
    num_tokens = 0
    for token in tokens_list:
        if token in model.wv:
            avg_vector += model.wv[token]
            num_tokens += 1
    if num_tokens > 0:
        avg_vector /= num_tokens
    return avg_vector

# Obtenir les vecteurs moyens pour chaque commentaire
X_vectorized = np.array([get_average_word2vec(comment, w2v_model, 100) for comment in tokenized_comments])

# Encoder les étiquettes
label_encoder = LabelEncoder()
y_encoded = label_encoder.fit_transform(data['success'])

# Diviser les données en ensembles d'entraînement et de test
X_train, X_test, y_train, y_test = train_test_split(X_vectorized, y_encoded, test_size=0.2, random_state=42)


# Construire le modèle de réseau de neurones
model = Sequential()
model.add(Input(shape=(X_train.shape[1],)))
model.add(Dense(128, activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(64, activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(1, activation='sigmoid'))  # 'sigmoid' pour une classification binaire

# Compiler le modèle
model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])

# Préparer TensorBoard
log_dir = "logs/fit/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
tensorboard_callback = TensorBoard(log_dir=log_dir, histogram_freq=1)

# Entraîner le modèle
early_stopping = EarlyStopping(monitor='val_loss', patience=5)
history = model.fit(X_train, y_train, validation_split=0.2, epochs=100, batch_size=32, callbacks=[early_stopping, tensorboard_callback])

# Évaluer le modèle
loss, accuracy = model.evaluate(X_test, y_test)
print(f'Loss: {loss}, Accuracy: {accuracy}')

# Prédictions sur les données de test
y_pred = (model.predict(X_test) > 0.5).astype("int32")

####ajouter une colonne predictions au nouveau data frame####
# Obtenir les vecteurs moyens pour chaque commentaire dans le DataFrame original
data['X_vectorized'] = data['Contenu_nettoye'].apply(lambda x: get_average_word2vec(x.split(), w2v_model, 100))

# Convertir la liste de vecteurs en une matrice numpy
X_all_vectorized = np.array(data['X_vectorized'].tolist())

# Faire des prédictions sur toutes les données
data['prediction'] = (model.predict(X_all_vectorized) > 0.5).astype("int32")
# Recoder les prédictions : 0 = échec, 1 = réussite
data['prediction'] = data['prediction'].replace({0: 'echec', 1: 'reussite'})

# Créer un nouveau DataFrame avec les colonnes souhaitées
columns_to_keep = ['Titre', 'Utilisateur', 'Date', 'Contenu', 'Contenu_nettoye', 'prediction']
data_predict = data[columns_to_keep]

# Enregistrer le DataFrame dans le répertoire
output_file_path = './data_predictions.csv'
data_predict.to_csv(output_file_path, index=False)
# Afficher les premières lignes du DataFrame avec les prédictions
print(data_predict.head())

# Scores supplémentaires
precision = precision_score(y_test, y_pred)
recall = recall_score(y_test, y_pred)
f1 = f1_score(y_test, y_pred)
accuracy = accuracy_score(y_test, y_pred)

print("\nScores supplémentaires:")
print(f'Precision: {precision}')
print(f'Recall: {recall}')
print(f'F1-Score: {f1}')
print(f'Accuracy: {accuracy}')

# Rapport de classification
print("\nClassification Report:")
print(classification_report(y_test, y_pred, target_names=['failure', 'success']))

# Matrice de confusion
print("\nConfusion Matrix:")
cm = confusion_matrix(y_test, y_pred)
sns.heatmap(cm, annot=True, fmt='d', cmap='Blues', xticklabels=['Failure', 'Success'], yticklabels=['Failure', 'Success'])
plt.xlabel('Predicted')
plt.ylabel('True')
plt.show()

# Afficher quelques prédictions sur les commentaires
sample_indices = np.random.choice(X_test.shape[0], 50, replace=False)  # Échantillons aléatoires pour affichage
sample_comments = data.iloc[sample_indices]['Contenu']
sample_predictions = y_pred[sample_indices]

print("\nPrédictions sur quelques commentaires:")
print("Prédits comme 'Succès':")
for comment, prediction in zip(sample_comments, sample_predictions):
    if prediction == 1:
        print(f"Comment: {comment}")
        print(f"Prediction (0 = Failure, 1 = Success): {prediction}")
        print("-" * 50)

print("\nPrédits comme 'Échec':")
for comment, prediction in zip(sample_comments, sample_predictions):
    if prediction == 0:
        print(f"Comment: {comment}")
        print(f"Prediction (0 = Failure, 1 = Success): {prediction}")
        print("-" * 50)


# Figure
plt.figure(figsize=(20, 20))

# Courbe d'apprentissage
plt.subplot(2, 2, 1)
plt.plot(history.history['loss'], label='Train Loss')
plt.plot(history.history['val_loss'], label='Validation Loss')
plt.title('Courbe d\'apprentissage')
plt.xlabel('Epochs')
plt.ylabel('Loss')
plt.legend()
plt.show()

# Courbe d'accuracy
plt.subplot(2, 2, 2)
plt.plot(history.history['accuracy'], label='Train Accuracy')
plt.plot(history.history['val_accuracy'], label='Validation Accuracy')
plt.title('Courbe de l\'accuracy')
plt.xlabel('Epochs')
plt.ylabel('Accuracy')
plt.legend()
plt.show()

